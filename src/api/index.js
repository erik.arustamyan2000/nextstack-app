import axios from "axios";

export const cdnBaseURL = 'https://propertyvision.nextstack.org/'

const api = axios.create({
    baseURL: 'https://propertyvision.nextstack.org/api/',
})

export const getServices = () => {
    return api.get('services').then(response => response.data.data.items)
}

export const getServiceDetails = (id) => {
    return api.get(`services/content/${id}`).then(response => response.data.data.items)
}

export const getServiceCategories = (id) => {
    return api.get(`services/categories/${id}`).then(response => response.data.data.items)
}

export const getServiceMedia = (id) => {
    return api.get(`services/works/${id}`).then(response => response.data.data.items)
}