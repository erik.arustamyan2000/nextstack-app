import React from 'react'
import Icon from '../Icon'
import { cdnBaseURL } from '../../api'
import './style.css'

const MEDIA_STATE_INITIAL = 'initial'
const MEDIA_STATE_PLAYING = 'playing'

export default class Gallery extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            activeMediaIndex: 0,
            mediaState: MEDIA_STATE_INITIAL
        }

        this.thumbRef = React.createRef()
    }

    componentDidUpdate(props) {
    }

    selectMedia = (index) => {
        const { activeMediaIndex } = this.state
        const thumbItemWidth = this.thumbRef.current.children[0].clientWidth
        if(Math.abs(index - activeMediaIndex) >= 1) {
            this.thumbRef.current.scrollLeft = (index - 1) * (thumbItemWidth + 15)
        }
        this.setState({activeMediaIndex: parseInt(index)})
        this.setState({mediaState: MEDIA_STATE_INITIAL})
    }

    handleNextClick = () => {
        const { activeMediaIndex } = this.state
        this.selectMedia(activeMediaIndex + 1 < this.props.media.length? activeMediaIndex + 1:0)
    }

    handlePrevClick = () => {
        const { activeMediaIndex } = this.state
        this.selectMedia(activeMediaIndex - 1 >= 0? activeMediaIndex - 1:this.props.media.length - 1)
    }

    handlePlayClick = () => {
        this.setState({mediaState: MEDIA_STATE_PLAYING})
    }

    render() {
        const { activeMediaIndex, mediaState } = this.state
        const { media } = this.props
        const activeItem = media && media.length? media[activeMediaIndex]:{}
        return (
            <div className='gallery-container'>
                <div className='active-media-container'>
                    {
                        mediaState === MEDIA_STATE_INITIAL? 
                        <div className='media-img'>
                            <img className='media' src={`${cdnBaseURL}${activeItem.image}`} />
                            <div className='play-btn' onClick={this.handlePlayClick}><Icon name='play' type='svg' /></div>
                        </div>:<video className='media' src={activeItem.vimeo} type='video/mp4' controls autoPlay/>
                    }
                    <div className='ctrl-btn left'></div>
                    <div className='ctrl-btn right'></div>
                </div>
                <div className='thumb-bar'>
                    <div className='thumb-ctrl-container'>
                        <div className='ctrl-inner'>
                            <div className='ctrl-btn left' onClick={this.handleNextClick}>{'>'}</div>
                            <div className='ctrl-btn right' onClick={this.handlePrevClick}>{'<'}</div>
                        </div>
                    </div>
                    <div className='thumb-container' ref={this.thumbRef}>
                        {
                            media && media.length? media.map((item, index) => {
                                return (
                                    <div key={item.id} className={`thumb-item ${activeMediaIndex === index? 'active':''}`} onClick={() => this.selectMedia(index)}>
                                        <img src={`https://propertyvision.nextstack.org/${item.image}`} />
                                    </div>
                                )
                            }):false
                        }
                    </div>
                </div>
            </div>
        )
    }
}