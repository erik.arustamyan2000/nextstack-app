import './style.css'

export default function Icon(props) {
    let element = false
    if (['svg', 'gif'].includes(String(props.type))) {
        element = <img className='svg' src={`/img/icon/${props.name}.${props.type}`} />
    }

    return <div className={`simple-icon ${props.className?? ''} ${props.name?? ''}`} >{element}</div>
}