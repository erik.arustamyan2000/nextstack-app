import ActionButton from '../ActionButton'
import ScaleText from 'react-scale-text'

import './style.css'

export default function OrderForm(props) {
    const { service } = props
    return (
        <div className={`order-form ${props.className?? ''}`}>
            <div className='form-inner'>
                <div>
                    <span className='price-label'><ScaleText minFontSize={14} maxFontSize={14}>Price</ScaleText></span> 
                    <span className='price'><ScaleText minFontSize={18} maxFontSize={20}>{service.start_at}$</ScaleText></span></div>
                <div className='title'><ScaleText minFontSize={20} maxFontSize={24}>{service.seo_title}</ScaleText></div>
                <div className='description'>
                    <ScaleText minFontSize={12} maxFontSize={12}>
                        {service.seo_description}
                    </ScaleText>
                </div>
                <ActionButton className='order-button' variant='primary'><ScaleText minFontSize={12} maxFontSize={13}>Order now</ScaleText></ActionButton>
                <ActionButton className='hiw-button' variant='secondary' icon=''>How it works</ActionButton>
            </div>
        </div>
    )
}