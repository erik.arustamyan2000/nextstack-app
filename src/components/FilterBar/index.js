import Grid from '@material-ui/core/Grid'
import ScaleText from 'react-scale-text'
import FilterButton from './FilterButton'

import './style.css'

export default function FilterBar(props) {
    const { filters } = props
    return filters && filters.length? (
        <Grid className='filter-bar' container>
            <Grid className='label' item><ScaleText minFontSize={16} maxFontSize={18}>Filter by</ScaleText></Grid>
            <Grid item><FilterButton className='filter-item' active={true}>All</FilterButton></Grid>
            {
                filters && filters.length? filters.map(filter => (
                    <Grid key={filter.name} item><FilterButton className='filter-item'>{filter.name}</FilterButton></Grid>
                )):false
            }
        </Grid>
    ):false
}