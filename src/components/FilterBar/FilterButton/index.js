import Button from '@material-ui/core/Button'

import './style.css'

export default function FilterButton(props) {

    return (<Button className={`filter-btn ${props.className?? ''} ${props.active? 'active':''}`}>{props.children}</Button>)
}