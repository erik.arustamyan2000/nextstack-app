import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import PageService from '../PageService'
import './global.css'
import './content.css'

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
            <Route exact path="/" render={props => <PageService {...props} />} />
            <Route exact path="/:service_id" render={props => <PageService {...props} />} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
