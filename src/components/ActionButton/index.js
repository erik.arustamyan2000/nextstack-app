import Button from '@material-ui/core/Button'

import './style.css'

/* Available button variants - primary, secondary*/
export default function ActionButton(props) {
    let iconProp = {}
    if (props.icon) {
        iconProp.startIcon = props.icon
    }
    return <Button className={`action-btn ${props.className?? ''} ${props.variant?? ''}`} {...iconProp}>{props.children}</Button>
}