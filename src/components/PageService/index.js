import { PureComponent } from "react"
import { withRouter } from 'react-router-dom'
import ScaleText from 'react-scale-text'
import Grid from '@material-ui/core/Grid'
import ServiceBar from '../ServiceBar'
import FilterBar from '../FilterBar'
import OrderForm from '../OrderForm'
import Gallery from '../Gallery'
import Icon from '../Icon'
import { getServices, getServiceDetails, getServiceCategories, getServiceMedia } from '../../api'

import './style.css'

class PageService extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            services: [],
            activeServiceDetails: {},
            activeServiceCategories: [],
            activeServiceMedia: []
        }
    }

    upadateData = async (props) => {
        const { service_id } = props.match.params
        let service_real_id = false
        await getServices().then(services => {
            this.setState({services})
            service_real_id = service_id? service_id:services.length? parseInt(services[0].id):false
        })
        if (service_real_id) {
            await getServiceCategories(service_real_id).then(activeServiceCategories => this.setState({activeServiceCategories}))
            await getServiceMedia(service_real_id).then(activeServiceMedia => this.setState({activeServiceMedia}))
            await getServiceDetails(service_real_id).then(activeServiceDetails => this.setState({activeServiceDetails}))
        }
    }

    componentDidMount() {
        this.upadateData(this.props)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.match.params.service_id !== this.props.match.params.service_id) {
            this.upadateData(nextProps)
        }
    }

    render() {
        const { services, activeServiceDetails, activeServiceCategories, activeServiceMedia } = this.state
        const { service_id } = this.props.match.params
        return (
            <div className='page-service'>
                <Grid container>
                    <Grid xs={12} md={2} item>
                        <ServiceBar services={services} activeServiceId={service_id} />
                    </Grid>
                    <Grid xs={12} md={10} item>
                        {
                            Object.keys(activeServiceDetails).length && (!service_id || activeServiceDetails.service_id == service_id)? (
                                <div className='content-container'>
                                    <div className='content-body'>
                                        <div className='page-title'><ScaleText minFontSize={16} maxFontSize={30}>{activeServiceDetails.page_name?? ''}</ScaleText></div>
                                        <FilterBar filters={activeServiceCategories} />
                                        <Grid className='main-section' container>
                                            <Grid xs={12} md={6} lg={5} xl={5} className='form-grid' item><OrderForm className='service-order-form' service={activeServiceDetails} /></Grid>
                                            <Grid xs={12} md={6} lg={7} xl={7} className='gallery-grid' item><Gallery media={activeServiceMedia} /></Grid>
                                        </Grid>
                                    </div>
                                </div>
                            ):<Icon className='page-loader' name='loader' type='gif' />
                        }
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default withRouter(PageService)