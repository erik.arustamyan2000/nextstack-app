import ScaleText from 'react-scale-text'
import { Link } from 'react-router-dom'
import { cdnBaseURL } from '../../api'
import './style.css'

export default function ServiceBar(props) {
    const { services, activeServiceId } = props
    return (
        <div className='service-bar-container'>
            {
                services && services.length? services.map(service => {
                    let isActive = service.id === parseInt(activeServiceId)
                    let icon = `${cdnBaseURL}${isActive? service.active_icon:service.icon}`

                    return (
                        <Link className={`service-item ${isActive? 'active':''}`} key={service.id} to={`/${service.id}`}>
                            <div className='item-inner'>
                                <div className='item-icon'><img src={icon} /></div>
                                <div className='item-label'><ScaleText minFontSize={12} maxFontSize={18}>{service.title}</ScaleText></div>
                            </div>
                        </Link>

                )}):false
            }
        </div>
    )
}